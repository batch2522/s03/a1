-- login to the mysql command:
mysql -u root -p

-- creating the database:
CREATE DATABASE db_name;

-- using the database:
USE db_name;

-- To create the tables for data insertion:
CREATE TABLE Users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    datetime_created DATETIME NOT NULL
);
CREATE TABLE Posts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    User_id INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    content VARCHAR(255) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    FOREIGN KEY (User_id) REFERENCES Users(id)
);

-- To add the records to the blog_db database:
INSERT INTO Users (email, password, datetime_created) VALUES
('johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00'),
('juandelacruz@gmail.com', 'passwordB', '2021-01-01 02:00:00'),
('janesmith@gmail.com', 'passwordC', '2021-01-01 03:00:00'),
('mariadelacruz@gmail.com', 'passwordD', '2021-01-01 04:00:00'),
('johndoe@gmail.com', 'passwordE', '2021-01-01 05:00:00');

INSERT INTO Posts (User_id, title, content, datetime_posted) VALUES
(1, 'First Code', 'Hello World!', '2021-01-02 01:00:00'),
(1, 'Second Code', 'Hello Earth!', '2021-01-02 02:00:00'),
(2, 'Third Code', 'Welcome to Mars!', '2021-01-02 03:00:00'),
(4, 'Fourth Code', 'Bye bye solar system!', '2021-01-02 04:00:00');

-- To get all the posts with an User id of 1:
SELECT * FROM Posts WHERE User_id = 1;

-- To get all the user's email and datetime of creation:
SELECT email, datetime_created FROM Users;

-- To update a post's content:
UPDATE Posts SET content = 'Hello to the people of the Earth!' WHERE content = 'Hello Earth!';

-- To delete the user with an email of "johndoe@gmail.com":
DELETE FROM Users WHERE email = 'johndoe@gmail.com';